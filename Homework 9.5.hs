myfoldl f acc [] = acc
myfoldl f acc (x:xs) = myfoldl f (f acc x) xs

main = print (myfoldl (+) 0 [1, 2, 3])