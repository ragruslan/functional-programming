minlist' [] m = m
minlist' (x:xs) m = if x < m then minlist' xs x else minlist' xs m
minlist xs = minlist' xs (head(xs))

main = print (minlist [3, 2, 7])