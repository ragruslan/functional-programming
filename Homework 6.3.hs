data Tree = Empty | Node Integer Tree Tree

height (Empty) = 0
height (Node val Empty Empty) = 0
height (Node val l r) = 1 + max (height(l)) (height(r))

main = print (height (Node 1 Empty Empty))