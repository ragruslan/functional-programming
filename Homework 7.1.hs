euclid 0 b = (0, 1)
euclid a b = (snd(e) - (div b a) * fst(e), fst(e)) where e = euclid (mod b a) a

main = print(euclid 3 5)