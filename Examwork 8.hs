primes = 2 : 3 : sieve [6 * k + l | k <- [1..], l <- [-1, 1]] where sieve (x : xs) = x : sieve [ a | a <- xs, mod a x /= 0]

main = print (take 30 primes)