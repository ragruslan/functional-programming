checkSum [] = False
checkSum (x:xs) = if or(map (\i -> i == 10) (map (\i -> i + x) xs)) then True else checkSum xs

main = print (checkSum [])