repeatFunc f 1 = f
repeatFunc f n = f . repeatFunc f (n - 1)

main = print (repeatFunc sin 2 1)