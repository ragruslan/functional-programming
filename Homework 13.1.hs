allLists n k = [[1 + mod (div x y) n | y <- nk] | x <- [0..(n^k - 1)]] where nk = [n^x | x <- [0..(k - 1)]]

main = print (allLists 3 2)