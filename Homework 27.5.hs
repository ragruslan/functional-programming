data Tree = Node Integer Tree Tree | Empty

toList' (Empty) acc = acc
toList' (Node val l r) acc = val : toList' l (toList' r acc)
toList t = toList' t []

main = print (toList (Node 1 (Node 2 Empty Empty) (Node 3 Empty Empty)))