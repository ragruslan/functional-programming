import Data.Foldable
import Prelude hiding (foldr, sum)

data Matrix a = Matr [[a]]

instance Foldable Matrix where
    foldr f acc (Matr xss) = foldr (\x y -> foldr f y x) acc xss

--main = print(foldr (+) 0 (Matr [[1, 2], [3, 4]]))
main = print(sum (Matr [[1, 2], [3, 4]]))