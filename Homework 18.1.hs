approxSum (x:xs) = if abs x < 0.000001 then 0 else x + approxSum xs
z = approxSum [1 / (n ^ 2) | n <- [1..]]

main = print (z)