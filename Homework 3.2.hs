minsum' [x] m = m
minsum' (x1:x2:xs) m = if x1 + x2 < m then minsum' (x2:xs) (x1 + x2) else minsum' (x2:xs) m
minsum (x:xs) = minsum' (x:xs) (x + head(xs))

main = print (minsum [2, 3])