sumprod xs = sum (map (\(x, y) -> x * y) (zip xs (tail(xs))))

main = print (sumprod [1,3,2,7])