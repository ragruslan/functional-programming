class Shape a where
    contains:: a -> Double -> Double -> Bool
    area:: a -> Double
    perim:: a -> Double


data Rect = Rect Double Double Double Double
data Circle = Circle Double Double Double

instance Shape Rect where
    contains (Rect a b x0 y0) x y = abs (x - x0) <= a/2 && abs (y - y0) <= b/2
    area (Rect a b _ _) = a * b
    perim (Rect a b x0 y0) = 2 * (a + b)

instance Shape Circle where
    contains (Circle r x0 y0) x y = (x - x0) ^ 2 + (y - y0) ^ 2 <= r ^ 2
    area (Circle r x0 y0) = pi * r ^ 2
    perim (Circle r x0 y0) = 2 * pi * r

main = print(contains (Circle 2 0 0) (0) (-2.01))