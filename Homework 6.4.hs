data Good = Cake String Double | Candy String Double Double

price (Cake name val) = val
price (Candy name val weight) = val * weight
totalPrice xs = sum (map (\x -> price(x)) xs)

test = totalPrice [Candy "void" 128 2.5, Candy "hacker" 2 256, Cake "kaka" 168]

main = print (test)