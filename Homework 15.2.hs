sumDiv n = (sum [x | x <- [2..(div n 2)], mod n x == 0]) + 1
xs n = [(a, sumDiv a) | a <- [2..n]]
amicables n = [(fst(a), fst(b)) | let xsn = xs n, a <- xsn, b <- xsn, snd(a) == fst(b), snd(b) == fst(a), fst(a) < fst(b)]

main = print (amicables 1000)