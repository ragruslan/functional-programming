data Tree = Empty | Node Integer Tree Tree

minHeight' ((Node _ Empty Empty), cur) _     = cur
minHeight' ((Node _ l     Empty), cur) queue = minHeight' (head(a)) (tail(a)) where a = queue++[(l, cur + 1)]
minHeight' ((Node _ Empty r    ), cur) queue = minHeight' (head(a)) (tail(a)) where a = queue++[(r, cur + 1)]
minHeight' ((Node _ l     r    ), cur) queue = minHeight' (head(a)) (tail(a)) where a = queue++[(l, cur + 1), (r, cur + 1)]
minHeight  node                              = minHeight' (node, 0) []

main = print (minHeight (Node 1 (Node 2 Empty Empty ) Empty ))