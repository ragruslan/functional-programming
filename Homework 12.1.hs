frame n = [a] ++ [b | x <- [1..(n - 2)]] ++ [a] where a = [1 | x <- [1..n]]
                                                      b = [if x /= 1 && x /= n then 0 else 1 | x <- [1..n]]

main = print (frame 50)