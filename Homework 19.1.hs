pascal = [1] : iterate (\x -> 1 : zipWith (+) (x) (tail x) ++ [1]) [1, 1]

main = print (take 10 pascal)