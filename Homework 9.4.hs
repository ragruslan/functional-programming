countOdd xs = foldr (\x y -> y + mod x 2) 0 xs
countOdd1 xs = sum (map (\x -> mod x 2) xs)

main = print (countOdd1 [1, 3, 5])