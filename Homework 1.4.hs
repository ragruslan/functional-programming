sumfact' 0 s p k = s
sumfact' n s p k = sumfact' (n - 1) (s + p * k) (p * k) (k + 1)
sumfact n = sumfact' n 0 1 1

main = print (sumfact 3)