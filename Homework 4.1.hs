upDown' [x] up = not up
upDown' (x:xs) up = if up then (if x < head(xs) then upDown' xs True else (if x /= head(xs) then upDown' xs False else False)) else (if x > head(xs) then upDown' xs False else False)
upDown (x:xs) = if x < head(xs) then upDown' xs True else False

main = print (upDown [1, 2])