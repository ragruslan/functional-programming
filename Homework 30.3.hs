allDiffLists' n 0 _ = [[]]
allDiffLists' n k s = [x:xs | x <- [1..n], s x, xs <- allDiffLists' n (k - 1) (\y -> y /= x && s y)]
allDiffLists n k = allDiffLists' n k (\x -> True)

main = print (allDiffLists 3 3)