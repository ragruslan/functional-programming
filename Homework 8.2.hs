euclid 0 b = (0, 1)
euclid a b = (snd(e) - (div b a) * fst(e), fst e) where e = euclid (mod b a) a

dioph' [0, b, c, d] [k, 0, 0, n] = [div n k, y, z] where y = signum(b) * fst(e) * g
                                                         z = signum(c) * snd(e) * g
                                                         e = euclid (abs b) (abs c)
                                                         g = div d (gcd b c)

dioph' [a, b, c, d] [k, l, m, n] = [fst(y) * e, snd(y) * e, div (n - k * fst(y) * e - l * (snd(y)) * e) m] where e = if g /= 0 then div (m*d - n*c) g else 0
                                                                                                                 y = (signum(a1) * fst(x), signum(b1) * snd(x))
                                                                                                                 x = euclid (abs a1) (abs b1)
                                                                                                                 g = gcd (m*a - k*c) (m*b - l*c)
                                                                                                                 a1 = m*a - k*c
                                                                                                                 b1 = m*b - l*c

dioph [a, b, c, d] [0, 0, 0, 0] = if      a * b /= 0 && mod d (gcd a b) == 0 then [signum(a) * fst(e1) * g1, signum(b) * snd(e1) * g1, 0]
                                  else if a * c /= 0 && mod d (gcd a c) == 0 then [signum(a) * fst(e2) * g2, 0, signum(c) * snd(e2) * g2]
                                  else if b * c /= 0 && mod d (gcd b c) == 0 then [0, signum(b) * fst(e3) * g3, signum(c) * snd(e3) * g3]
                                  else if                             a /= 0 then [div d a, 0, 0]
                                  else if                             b /= 0 then [0, div d b, 0]
                                  else if                             c /= 0 then [0, 0, div d c]
                                  else [0, 0, 0] where e1 = e (a, b)
                                                       g1 = div d (gcd a b)
                                                       e2 = e (a, c)
                                                       g2 = div d (gcd a c)
                                                       e3 = e (b, c)
                                                       g3 = div d (gcd b c)
                                                       e = (\(x, y) -> euclid (abs x) (abs y))

dioph [0, 0, 0, 0] [a, b, c, d] = dioph [a, b, c, d] [0, 0, 0, 0]

dioph [a, b, c, d] [k, l, m, n] = if            m * c /= 0 then                             dioph' [a, b, c, d] [k, l, m, n]
                                  else if       l * b /= 0 then ((\[x, y, z] -> [x, z, y]) (dioph' [a, c, b, d] [k, m, l, n]))
                                  else if       k * a /= 0 then ((\[x, y, z] -> [z, y, x]) (dioph' [c, b, a, d] [m, l, k, n]))
                                  else if m == 0 && l == 0 then                             dioph' [0, b, c, d] [k, 0, 0, n]
                                  else if m == 0 && k == 0 then ((\[x, y, z] -> [y, x, z]) (dioph' [0, a, c, d] [l, 0, 0, n]))
                                  else if l == 0 && k == 0 then ((\[x, y, z] -> [y, z, x]) (dioph' [0, a, b, d] [m, 0, 0, n]))
                                  else                                                      dioph  [k, l, m, n] [a, b, c, d]

main = print(dioph [-7, 2, 0, -1] [0, 0, 0, 0] )
--main = print (euclid (-36) (-36))