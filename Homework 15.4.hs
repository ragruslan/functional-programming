data Scheme = Resistor Double | Parallel Scheme Scheme | Serial Scheme Scheme

totalResistance (Resistor r) = r
totalResistance (Parallel a b) = r1 * r2 / (r1 + r2) where r1 = totalResistance a
                                                           r2 = totalResistance b

totalResistance (Serial a b) = (totalResistance a) + (totalResistance b)

foldScheme f e (Resistor r) = f 0 e r
foldScheme f e (Parallel a b) = f 1 (foldScheme f e a) (foldScheme f e b)
foldScheme f e (Serial a b) = f 2 (foldScheme f e a) (foldScheme f e b)

test1 = foldScheme (\x y z -> if x == 0 || x == 2 then y + z else y * z / (y + z)) 0 (Parallel (Serial (Resistor 2) (Resistor 2)) (Resistor 2)) --4/3
test2 = foldScheme (\x y z -> if x == 0 then 1 else y + z) 0 (Parallel (Serial (Resistor 2) (Resistor 2)) (Resistor 2)) --3
test3 = foldScheme (\x y z -> if x == 0 then y else 1 + max y z) (-1) (Parallel (Serial (Resistor 2) (Resistor 2)) (Resistor 2)) --1

main = print (test1, test2, test3)