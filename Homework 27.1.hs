data Tree = Node Integer Tree Tree | Empty

find p (Empty) err = err
find p (Node val l r) err = if p val then val else find p l (find p r err)

main = print (find (>3) ( Node 5 (Node 4 Empty Empty) (Node 3 Empty Empty)) 0)