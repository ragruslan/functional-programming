--generalizedCantor' k = 
generalizedCantor n = [(a - b - c + 2, b - c + 1, c) | a <- [1..], b <- [1..a], c <- [1..min b (a - b + 1)]]

main = print (take 200 $ generalizedCantor 3)