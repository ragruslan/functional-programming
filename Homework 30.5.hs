find cond (x:xs) = if cond x then (x, xs) else find cond xs

f >>> g = f . snd . g

f = find (>3) >>> find (>3)

main = print (f [1, 3, 5, 2, 20, 25, 2])