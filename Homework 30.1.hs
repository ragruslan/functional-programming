sumSqr_cps [x] f = f (x^2)
sumSqr_cps (x:xs) f = sumSqr_cps xs (f . (x^2+))

main = print(sumSqr_cps [1, 2, 3] (*2))