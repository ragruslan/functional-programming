data Tree = Empty | Node Integer Tree Tree

foldTree' f (Node x Empty Empty) = x
foldTree' f (Node x Empty r) = f (foldTree' f r) x
foldTree' f (Node x l Empty) = f (foldTree' f l) x
foldTree' f (Node x l r) = f (foldTree' f l) (f (foldTree' f r) x)
foldTree  f acc node = f (foldTree' f node) acc


main = let x = (foldTree (+) 3 (Node 1 (Node 256 (Node 256 Empty Empty) Empty) (Node 2 Empty Empty) ) ) --3 + 1 + 256 + 256 + 2 = 518
           y = (foldTree (*) 2 (Node 1 (Node 256 (Node 4 (Node 8 Empty Empty) Empty) Empty) (Node 16 Empty Empty) ) ) in print [x, y] --2 * 1 * 256 * 4 * 8 * 16 = 262144