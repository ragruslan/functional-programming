cartesian xs ys = xs >>= \x -> (ys >>= \y -> [(x, y)])

main = print (cartesian [1,2] [3,4])