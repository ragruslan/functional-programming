coins n = [[div a 2, l, m] | m <- [0..(div n 5)], l <- [0..(div (n - m * 5) 3)], let a = (n - (l * 3 + m * 5)), mod a 2 == 0, a >= 0]

main = print (coins 9)