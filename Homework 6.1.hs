isosc (x1, y1) (x2, y2) (x3, y3) = (a - b) == 0 || (a - c) == 0 || (b - c) == 0 where 
                                                                                     a = (x1 - x2) ^ 2 + (y1 - y2) ^ 2
                                                                                     b = (x1 - x3) ^ 2 + (y1 - y3) ^ 2
                                                                                     c = (x2 - x3) ^ 2 + (y2 - y3) ^ 2

main = print(isosc (-3, 2) (2, 0) (2,2))