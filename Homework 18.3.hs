brackets = "[]" : [('[' : x) ++ "]" | x <- brackets]

main = print (take 10 brackets)