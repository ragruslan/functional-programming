import           Data.List

sieve xs n = filter (\x -> (x <= n) || mod x n /= 0) xs
primes xs [] = xs
primes xs (y:ys) = primes (y:xs) (sieve ys y)
almostPrimes res xs [] = res
almostPrimes res xs ys = almostPrimes ((map (\(x, y) -> x * y) (zip xs ys))++res) xs (tail(ys))
g' _ [] = False
g' pr1 (x:pr2) = if (any (0==) (map (\n -> n - x) pr1)) then True else g' pr1 pr2
g n = g' (filter (>0) (map (\x -> n - x) a)) a where a = sort (almostPrimes [] b b)
                                                     b = (primes [] [2..(div n 2)])

main = print(g 19089)