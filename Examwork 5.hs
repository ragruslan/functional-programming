applyList xs x = (foldr (.) (const x) xs) x

main = print (applyList [sin, cos] 0)