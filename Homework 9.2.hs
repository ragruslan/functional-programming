multTable n = map (\x -> map (\i -> i * x) [1..n]) [1..n]

main = print (multTable 4)