--transpose xss = foldl (\x y -> foldl (\z w -> ) x y) ([[] | x <- [1..length xss]]) xss
--transpose xss = map (\(x, y) -> (x ++ [(head y)], tail y)) (zip ([[] | x <- [1..length xss]]) xss)
transpose xss = foldl (\x y -> map (\(z, w) -> z ++ [w]) (zip x y)) ([[] | x <- [1..length xss]]) xss

main = print (transpose [[1,2,3],[4,5,6],[7,8,9]])