check cond xs = or(map cond xs)

main = print(check (\x -> x > 10) [2, 5, 7, 11, 4, 6, 3, 10])