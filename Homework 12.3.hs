data Scheme = Resistor Double | Parallel Scheme Scheme | Serial Scheme Scheme

totalResistance (Resistor r) = r
totalResistance (Parallel a b) = r1 * r2 / (r1 + r2) where r1 = totalResistance a
                                                           r2 = totalResistance b

totalResistance (Serial a b) = (totalResistance a) + (totalResistance b)

test = totalResistance (Serial (Parallel (Resistor 4) (Resistor 4) ) (Resistor 3) )

main = print (test)