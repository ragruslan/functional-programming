doubleEven xs = xs >>= \x -> if even x then [x, x] else [x]

main = print (doubleEven [1, 2, 6, 3])