sumDiv n = (sum [x | x <- [2..(div n 2)], mod n x == 0]) + 1
perfects n = [x | x <- [2..n], sumDiv x == x]

main = print (perfects 1000)