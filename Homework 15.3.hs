data Tree = Node Integer Tree Tree | Empty

foldTree f e (Node val l r) = let resl = foldTree f e l
                                  resr = foldTree f e r
                                  in f val resl resr
foldTree f e Empty = e
sumSqr = foldTree (\x y z -> x^2 + y + z) 0

main = print (sumSqr (Node 3 (Node (-1) Empty Empty) (Node 4 Empty Empty)))