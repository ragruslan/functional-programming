find cond [] = []
find cond (x:xs) = if cond x then [x] else find cond xs

--f xs = foldr (\x acc -> if acc == [] || x == [] then [] else [head x + head acc]) [0] [find (odd) xs, find (<0) xs, find (>7) xs]

f xs = do x <- find (odd) xs
          y <- find (<0) xs
          z <- find (>7) xs
          return (x + y + z)

main = print (f [3, 6, 4, 3, 20])