checkSumMono' [] _ = False
checkSumMono' _ [] = False
checkSumMono' (x:xs) (y:ys) = if x == y then False else if x + y == 10 then True else if x + y > 10 then checkSumMono' (x:xs) ys else checkSumMono' xs (y:ys)
checkSumMono xs = checkSumMono' xs (reverse xs)

main = print(checkSumMono [-8, -6, -4, -2, 0, 1, 5, 7, 11, 15])