fib = 1:fib where fibA = 1:fib
                  fibB = zipWith (+) fib fibA
                  fib = 1:fibB

main = print(take 10 fib)