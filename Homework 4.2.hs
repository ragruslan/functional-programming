--falls [x] ys n = n:ys
--falls (x:xs) ys n = if (x > head(xs)) then falls xs (n:ys) (n + 1) else falls xs ys (n + 1)
--gcd' a b = if (b /= 0) then gcd' b (mod a b) else a
--gcdL [x] = x
--gcdL (x:y:xs) = gcdL ((gcd x y):xs)
--parts xs = (gcdL (falls xs [] 1)) > 1

--main = print (parts [1, 2, 2, 0, 1, 1])

parts' [x] g n = gcd g n
parts' (x:xs) g n = if (x >= head(xs)) then parts' xs (gcd g n) (n + 1) else parts' xs g (n + 1)
parts xs = (parts' xs 0 1) > 1

main = print (parts [1, 2, 2, 0, 1, 1])