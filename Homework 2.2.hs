p' n 0 = if n == 0 then 1 else 0
p' n k = if n >= k then p' (n) (k - 1) + p' (n - k) (k) else p' n n
p n = p' n n

main = print (p 9)