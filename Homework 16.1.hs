cantor = [(a - b + 1, b) | a <- [1..], b <- [1..a]]

main = print (take 100 cantor)