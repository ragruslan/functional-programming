superMap f [] = []
superMap f (x:xs) = f x ++ (superMap f xs)

main = print (superMap (\x -> [x*x, 100*x]) [1,2,3])