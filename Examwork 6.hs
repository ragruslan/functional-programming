repeatFunc f 1 = f
repeatFunc f n = f . repeatFunc f (n - 1)

xs = [1] ++ [repeatFunc sin 1 1, repeatFunc sin 2 1..]

xs1 = 1 : sin(-head xs1) : xs1

xs2 = 1 : [sin x | x <- xs2]
xs3 = 1 : [sin (-x) | x <- xs3]


main = print ((head $ tail $ tail $ tail xs2))