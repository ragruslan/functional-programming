powerset xs = foldr (\x ys -> ys ++ (map (x:) ys)) [[]] xs

main = print (powerset [1, 2, 3])